const menu = document.querySelector('#menu');
const sidebar = document.querySelector('.sidebar');

menu.addEventListener('click', function(){
    sidebar.classList.toggle('show-sidebar');
})

fetch("../video/videos.json")
.then(response => response.json())
.then(data => {
    console.log(data.videos)
    for(var i = 0; i<data.videos.length; i++){
        let newTitle = data.videos[i].title;
        let newAuthor = data.videos[i].author;
        let newdetails = data.videos[i].details;
        let newThumbnail = data.videos[i].thumbnail;
        let newIcon = data.videos[i].channelicon;
           document.querySelector("#videoContainer").innerHTML +=
           `<div class="video">
           <div class="videoThumbnail">
             <img src="../images/${newThumbnail}"/>
           </div>
           <div class="videoDetails">
             <div class="author">
               <img src="../images/${newIcon}"alt=""/>
             </div>
             <div class="title">
              <h3>${newTitle}</h3>
              <a href=""> ${newAuthor}</a>
              <span>${newdetails}</span>
            </div>
            </div>
        </div>`;
        
        
    }
})
// if video.json data doesnt work 
// heres alternative data

// let text = '{"videos":[' +
// '{"title":"Sample number 1 youtube video card","author":"Programmer Lesson1","details":"10M Views • 3 weeks ago","thumbnail": "thumnail1.jpg","channelicon":"thumnail20.jpg" },' +
// '{"title":"Sample number 2 youtube video card","author":"Programmer Lesson2","details":"40M Views • 4 weeks ago","thumbnail": "thumnail2.jpg","channelicon":"thumnail19.jpg" },' +
// '{"title":"Sample number 3 youtube video card","author":"Programmer Lesson3","details":"1M Views • 3 days ago","thumbnail": "thumnail3.jpg","channelicon":"thumnail18.jpg" },' +
// '{"title":"Sample number 4 youtube video card","author":"Programmer Lesson4","details":"18M Views • 15 hours ago","thumbnail": "thumnail4.jpg","channelicon":"thumnail17.jpg" },' +
// '{"title":"Sample number 5 youtube video card","author":"Programmer Lesson5","details":"3M Views • 3 Months ago","thumbnail": "thumnail5.jpg","channelicon":"thumnail16.jpg" },' +
// '{"title":"Sample number 6 youtube video card","author":"Programmer Lesson6","details":"5M Views • 4 days ago","thumbnail": "thumnail6.jpg","channelicon":"thumnail15.jpg" },' +
// '{ "title":"Sample number 7 youtube video card","author":"Programmer Lesson7","details":"7M Views • 5 days ago","thumbnail": "thumnail7.jpg","channelicon":"thumnail14.jpg" },' +
// '{"title":"Sample number 8 youtube video card","author":"Programmer Lesson8","details":"12M Views • 3 months ago","thumbnail": "thumnail8.jpg","channelicon":"thumnail13.jpg" },' +
// '{ "title":"Sample number 9 youtube video card","author":"Programmer Lesson9","details":"9M Views • 1 year ago","thumbnail": "thumnail9.jpg","channelicon":"thumnail12.jpg" },' +
// '{"title":"Sample number 10 youtube video card","author":"Programmer Lesson10","details":"12K Views • 12 months ago","thumbnail": "thumnail10.jpg","channelicon":"thumnail11.jpg" },' +
// '{"title":"Sample number 11 youtube video card","author":"Programmer Lesson11","details":"10K Views • 7 hours ago","thumbnail": "thumnail11.jpg","channelicon":"thumnail10.jpg" },' +
// '{"title":"Sample number 12 youtube video card","author":"Programmer Lesson12","details":"100K Views • 12 hours ago","thumbnail": "thumnail12.jpg","channelicon":"thumnail9.jpg" },' +
// '{"title":"Sample number 13 youtube video card","author":"Programmer Lesson13","details":"2K Views • 16 days ago","thumbnail": "thumnail13.jpg","channelicon":"thumnail8.jpg" },' +
// '{ "title":"Sample number 14 youtube video card","author":"Programmer Lesson14","details":"34K Views • 5 dyas ago","thumbnail": "thumnail14.jpg","channelicon":"thumnail7.jpg" },' +
// '{ "title":"Sample number 15 youtube video card","author":"Programmer Lesson15","details":"16K Views • 5 hours ago","thumbnail": "thumnail15.jpg","channelicon":"thumnail6.jpg" },' +
// '{"title":"Sample number 16 youtube video card","author":"Programmer Lesson16","details":"10M Views • 1 hour ago","thumbnail": "thumnail16.jpg","channelicon":"thumnail5.jpg" },' +
// '{ "title":"Sample number 17 youtube video card","author":"Programmer Lesson17","details":"11K Views • 1 week ago","thumbnail": "thumnail17.jpg","channelicon":"thumnail4.jpg" },' +
// '{"title":"Sample number 18 youtube video card","author":"Programmer Lesson18","details":"12K Views • 12 hours ago","thumbnail": "thumnail18.jpg","channelicon":"thumnail3.jpg" },' +
// '{"title":"Sample number 19 youtube video card","author":"Programmer Lesson19","details":"15K Views • 3 months ago","thumbnail": "thumnail19.jpg","channelicon":"thumnail2.jpg" },' +
// '{"title":"Sample number 20 youtube video card","author":"Programmer Lesson20","details":"23K Views • 2 years ago","thumbnail": "thumnail20.jpg","channelicon":"thumnail1.jpg" }]}';

// const obj = JSON.parse(text);
// for(var i = 0; i<obj.videos.length; i++){
//   let newTitle = obj.videos[i].title;
//   let newAuthor = obj.videos[i].author;
//   let newdetails = obj.videos[i].details;
//   let newThumbnail = obj.videos[i].thumbnail;
//   let newIcon = obj.videos[i].channelicon;
//   document.querySelector("#videoContainer").innerHTML +=
//            `<div class="video">
//            <div class="videoThumbnail">
//              <img src="../images/${newThumbnail}"/>
//            </div>
//            <div class="videoDetails">
//              <div class="author">
//                <img src="../images/${newIcon}"alt=""/>
//              </div>
//              <div class="title">
//               <h3>${newTitle}</h3>
//               <a href=""> ${newAuthor}</a>
//               <span>${newdetails}</span>
//             </div>
//             </div>
//         </div>`;
  
// }
